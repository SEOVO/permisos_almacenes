from odoo import models, fields, api

class User(models.Model):
    _inherit = 'res.users'
    warehouse_ids_it = fields.Many2many('stock.warehouse',string='Almacenes Permitidos')

class Almacen(models.Model):
    _inherit = 'stock.warehouse'
    warehouse_permit = fields.Boolean(default=False,compute="get_permit")
    #warehouse_ids_it = fields.Many2many('stock.warehouse', string='Almacenes Permitidos')
    def get_w_ids(self):
        return [1,2,3]
    @api.depends('warehouse_permit')
    def get_permit(self):
        for record in self:
            #record.warehouse_permit = True

            wrs = self.env.user.warehouse_ids_it
            #context = self._context

            #current_uid = context.get('uid')

            #user = self.env['res.users'].browse(current_uid)
            #wrs = user.warehouse_ids_it

            for w in wrs:
                if record.id == w.id:
                    record.warehouse_permit =  True
                else:
                    record.warehouse_permit = False


