# -*- encmodels.poding: utf-8 -*-
{
    'name': 'PERMISOS DE ALMACENES',
    'category': '',
    'author': 'ITGRUPO',
    'depends': ['stock'],
    'version': '1.0',
    'description':"""
    PERMISO DE ALMACENES
    """,
    'auto_install': False,
    'demo': [''],
    'data': [
        #'security/ir.model.access.csv',
        'views/views.xml',
         'views/rules.xml',
        ],
    'installable': True
}